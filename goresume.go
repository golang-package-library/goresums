package lib

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/afex/hystrix-go/hystrix"
	"io/ioutil"
	"log"
	"net/http"
)

type configGoresume struct {
	BaseUrl            string
	InsecureSkipVerify bool
	Payload            interface{}
	Method             string
	Header             []header
	QueryParams        map[string]interface{}
	CircuitBreaker     CircuitBreaker
}

type CircuitBreaker struct {
	HystrixTimeout               int
	HistrixMaxConcurrentRequests int
	HystrixErrorPercentThreshold int
}

type header struct {
	Key   string
	Value string
}

var errorResponse struct {
	Message string `json:"message"`
}

type Http struct {
	HttpRequest  *http.Request
	HttpHeader   *http.Header
	HttpClient   *http.Client
	HttpResponse *http.Response
}

func NewGoresume(baseUrl string, method string, token string, queryParams map[string]interface{}, cb CircuitBreaker) (response map[string]interface{}, err error) {
	var confGoresume configGoresume
	hystrix.ConfigureCommand("http", hystrix.CommandConfig{
		Timeout:               cb.HystrixTimeout,
		MaxConcurrentRequests: cb.HistrixMaxConcurrentRequests,
		ErrorPercentThreshold: cb.HystrixErrorPercentThreshold,
	})

	resultChan := make(chan map[string]interface{}, 1)
	errChan := hystrix.Go("http", func() error {
		response, err = newHttpRequest(method, baseUrl, confGoresume.Payload).
			setQueryParam(queryParams).
			setHeaders(token).doHttpClient(newHttpClient(true))
		if err != nil {
			fmt.Println(err)
			return err
		}
		resultChan <- response
		return nil
	}, nil)

	select {
	case result := <-resultChan:
		log.Println("success:", result)
		return result, err
	case errors := <-errChan:
		log.Println("Circuit breaker opened for http command", errors.Error())
		return response, err
	}

	return response, err
}

func newHttpRequest(method string, baseUrl string, payload interface{}) *Http {
	var httpRequest Http
	resByte, _ := json.Marshal(payload)
	request := bytes.NewBuffer(resByte)

	req, err := http.NewRequest(method, baseUrl, request)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	httpRequest.HttpRequest = req
	return &httpRequest
}

func (h *Http) setQueryParam(queryParams map[string]interface{}) *Http {
	q := h.HttpRequest.URL.Query()
	for k, v := range queryParams {
		q.Add(k, fmt.Sprintf("%v", v))
	}
	h.HttpRequest.URL.RawQuery = q.Encode()
	return h
}

func (h *Http) setHeaders(token string) *Http {
	headers := []header{
		{
			Key:   "Authorization",
			Value: token,
		},
	}
	h.HttpRequest.Header.Add("Content-Type", "application/json")
	for _, header := range headers {
		h.HttpRequest.Header.Add(header.Key, header.Value)
	}
	return h
}

func newHttpClient(insecureSkipVerify bool) *http.Client {
	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureSkipVerify}, // ignore expired SSL certificates
	}
	client := &http.Client{Transport: transCfg}
	return client
}

func (h *Http) doHttpClient(client *http.Client) (map[string]interface{}, error) {
	var response map[string]interface{}
	respHttp, err := client.Do(h.HttpRequest)

	if err != nil {
		fmt.Println(err)
		return response, err
	}
	defer respHttp.Body.Close()

	if respHttp.StatusCode != http.StatusOK {
		err = json.NewDecoder(respHttp.Body).Decode(&errorResponse)
		if err != nil {
			fmt.Println(err)
			return response, err
		}
		// handle 401
		fmt.Println("Error:", respHttp)
		return response, err
	}

	bodyBytes, err := ioutil.ReadAll(respHttp.Body)
	if err != nil {
		fmt.Print(err.Error())
		return response, err
	}

	json.Unmarshal(bodyBytes, &response)
	return response, err
}
